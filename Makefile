TARGET_DIR := target

NINJA_OPTS := -j 4

.PHONY: compile
compile: setup
	@ninja -C $(TARGET_DIR) $(NINJA_OPTS)

.PHONY: setup
setup: $(TARGET_DIR)

$(TARGET_DIR):
	@meson $(TARGET_DIR)

.PHONY: gen-gl3w
gen-gl3w: gl3w/gl3w.c

gl3w/gl3w.c:
	mkdir -p gl3w_out
	cd gl3w_out && python ../gl3w/gl3w_gen.py

.PHONY: clean
clean:
	@[ -d $(TARGET_DIR) ] && rm -rf $(TARGET_DIR) || true
